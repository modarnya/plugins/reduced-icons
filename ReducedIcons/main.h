#ifndef MAIN_H
#define MAIN_H

#include "loader/loader.h"

#include <SRDescent/SRDescent.h>
#include <llmo/SRHook.hpp>
#include <gtasa/CGame/Types.h>

class AsiPlugin : public SRDescent {
	public:
		explicit AsiPlugin();
	private:
		float scaleX = 0.5f;
		float scaleY = 0.5f;
		void DrawIconHook(SRHook::CPU &cpu, RwVRect *&rect);
		SRHook::Hook<RwVRect *>	drawIcon{ 0x5860F7, 5 };
};

#endif // MAIN_H
